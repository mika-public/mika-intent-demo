package id.mikaapp.mikaintentdemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import id.mikaapp.mikaintentdemo.MainMenuActivity.Companion.MIKA_INTENT_REQUEST_CODE
import kotlinx.android.synthetic.main.activity_settlement.*

class SettlementActivity : BaseChildActivity(R.layout.activity_settlement) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainMenuActivity.username?.let { username.setText(it) }
        MainMenuActivity.password?.let { password.setText(it) }
        settlement.setOnClickListener {
            try {
                val intent = Intent("id.mikaapp.mika.settlement").apply {
                    putExtra("USERNAME", username.text.toString())
                    putExtra("PASSWORD", password.text.toString())
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_CODE)
            } catch (e: Exception) {
                Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MIKA_INTENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                val errorCode = data?.getStringExtra("ERROR_CODE")
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    this,
                    "TRANSACTION FAILED\n $errorCode: $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
