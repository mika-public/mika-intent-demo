package id.mikaapp.mikaintentdemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import id.mikaapp.mikaintentdemo.MainMenuActivity.Companion.MIKA_INTENT_REQUEST_CODE
import kotlinx.android.synthetic.main.activity_void.*

class VoidActivity : BaseChildActivity(R.layout.activity_void) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainMenuActivity.lastSuccessTransactionId?.let { transactionId.setText(it) }
        MainMenuActivity.username?.let { username.setText(it) }
        MainMenuActivity.password?.let { password.setText(it) }
        voidButton.setOnClickListener {
            try {
                val intent = Intent("id.mikaapp.mika.void").apply {
                    putExtra("USERNAME", username.text.toString())
                    putExtra("PASSWORD", password.text.toString())
                    putExtra("TRANSACTION_ID", transactionId.text.toString())
                }
                startActivityForResult(intent, MIKA_INTENT_REQUEST_CODE)
            } catch (e: Exception) {
                Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MIKA_INTENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                requireNotNull(data)
                val approvalCode = data.getStringExtra("APPROVAL_CODE")
                Toast.makeText(
                    this,
                    "VOID SUCCESS\n" +
                            "Approval Code: $approvalCode",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (resultCode == Activity.RESULT_CANCELED) {
                val errorCode = data?.getStringExtra("ERROR_CODE")
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    this,
                    "TRANSACTION FAILED\n $errorCode: $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
