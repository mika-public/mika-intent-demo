# Changelog
All notable changes to this project will be documented in this file.

## [1.3.2] - 2020-07-07
### Added
  - New output parameters from Sale: `TRANSACTION_ALIAS`, `ACQUIRER_CLASS`, `ACQUIRER_COMPANY`, `AUTHORIZATION_TYPE`, `FIRST_SIX_LAST_FOUR_PAN`
  - New optional input parameter for Print: `COPY_ID`
### Changed
  - Visual updates to Void and Settlement screens.
